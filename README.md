# PinoyERPTraining - Dev Guide

## Technologies that we use:
1. Java 6^
2. MySQL
3. Maven
4. Spring Framework (MVC/Boot)
5. REST
6. Familiar in any Linux/Unix
7. SVN
8. Git
9. Unit Test (JUnit 4 and Mockito 2.x)

### Java Reference
1. https://github.com/akullpp/awesome-java
2. Effective Java any edition
3. Java in 21 days any edition

### MySQL
1. https://www.mysql.com/

### Maven
1. https://maven.apache.org/

### Spring Framework
1. https://projects.spring.io/spring-framework/

### REST
1. https://www.slideshare.net/stormpath/rest-jsonapis

### Familiar in any Linux/Unix
1. https://distrowatch.com/

### SVN
1. https://subversion.apache.org/

### GIT
1. https://git-scm.com/

## Installation

### Java
1. Download latest JDK

### MySQL
1. There are two options its either downloading the lates XAMPP Package or Standalone MySQL Server
	1. XAMPP - https://www.apachefriends.org/index.html
	1. MySQL - https://dev.mysql.com/downloads/mysql/

### Maven
1. https://www.youtube.com/watch?v=OfCTUGpWEdE

### GIT
1. https://git-scm.com/ - you have two options its either you have a visual client or terminal interface
	1. https://www.sourcetreeapp.com/ for visual
	1. Pre installed GIT bash bundled in GIT for windows for terminal

### SVN
1. https://tortoisesvn.net/

### IDE
1. Eclipse - install also the plugins
	1. Hibernate Manager
2. Intellij Community Edition or Ultimate if there is a budget

### Unit Test
1. JUnit - https://junit.org/junit5/
2. Mockito - https://site.mockito.org/
